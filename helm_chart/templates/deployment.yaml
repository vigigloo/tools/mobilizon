{{- $envVars := include "common.env.transformDict" .Values.envVars -}}
{{- $fullName := include "common.fullname" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      {{- include "common.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmaps.yaml") . | sha256sum }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        {{- include "common.selectorLabels" . | nindent 8 }}
    spec:
      {{- if $.Values.image.credentials }}
      imagePullSecrets:
        - name: {{ include "common.secret.dockerconfigjson.name" (dict "fullname" $fullName "imageCredentials" $.Values.image.credentials) }}
      {{- end}}
      serviceAccountName: {{ include "common.serviceAccountName" . }}
      initContainers:
        - name: {{ .Chart.Name }}-init-config
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          command:
            - /override/script/override.sh
          volumeMounts:
            - name: etc-mobilizon
              mountPath: /override/etc-mobilizon
            - name: overrides
              mountPath: /override/overrides
            - name: override-script
              mountPath: /override/script/override.sh
              subPath: override.sh
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          {{- if $envVars}}
          env:
            {{- $envVars | indent 12 }}
          {{- end }}
          ports:
            - name: http
              containerPort: {{ .Values.service.targetPort }}
              protocol: TCP
            {{- if .Values.monitoring.exporter.enabled }}
            - name: metrics
              containerPort: {{ .Values.monitoring.exporter.port }}
              protocol: TCP
            {{- end }}
          {{- if .Values.probes.liveness }}
          livenessProbe:
            {{- if .Values.probes.liveness.exec }}
            exec:
            {{- toYaml .Values.probes.liveness.exec | nindent 14 }}
            {{- else if .Values.probes.liveness.tcpSocket }}
            tcpSocket:
            {{- toYaml .Values.probes.liveness.tcpSocket | nindent 14 }}
            {{- else }}
            httpGet:
              path: {{ .Values.probes.liveness.path }}
              port: {{ .Values.service.targetPort }}
            {{- end }}
            initialDelaySeconds: {{ .Values.probes.liveness.initialDelaySeconds | eq nil | ternary 60 .Values.probes.liveness.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.probes.liveness.timeoutSeconds | eq nil | ternary 10 .Values.probes.liveness.timeoutSeconds }}
          {{- end }}
          {{- if .Values.probes.readiness }}
          readinessProbe:
            {{- if .Values.probes.readiness.exec }}
            exec:
              {{- toYaml .Values.probes.readiness.exec | nindent 14 }}
            {{- else if .Values.probes.readiness.tcpSocket }}
            tcpSocket:
            {{- toYaml .Values.probes.readiness.tcpSocket | nindent 14 }}
            {{- else }}
            httpGet:
              path: {{ .Values.probes.readiness.path }}
              port: {{ .Values.service.targetPort }}
            {{- end }}
            initialDelaySeconds: {{ .Values.probes.readiness.initialDelaySeconds | eq nil | ternary 60 .Values.probes.readiness.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.probes.readiness.timeoutSeconds | eq nil | ternary 10 .Values.probes.readiness.timeoutSeconds }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: etc-mobilizon
              mountPath: /etc/mobilizon
            {{- range $name, $volume := .Values.persistence }}
            - name: "{{ $name }}"
              mountPath: "{{ $volume.mountPath }}"
            {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- tpl (. | toYaml) $ | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if .Values.persistence }}
      {{- with .Values.securityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: etc-mobilizon
          emptyDir: {}
        - name: overrides
          configMap:
            name: {{ $fullName }}-overrides
        - name: override-script
          configMap:
            name: {{ $fullName }}-override-script
            defaultMode: 0777
        {{- range $name, $volume := .Values.persistence }}
        - name: "{{ $name }}"
          persistentVolumeClaim:
            claimName: "{{ $fullName }}-{{ $name }}"
        {{- end }}
      {{- end }}
